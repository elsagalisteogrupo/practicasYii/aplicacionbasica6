<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrabajadoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trabajadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trabajadores-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Trabajadores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,//busquedas
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'apellidos',
            'fechaNacimiento',
            //'foto',
            [
                'label'=>'Foto',
                'format'=>'raw',
                'value'=>function($d){
        return Html::img("@web/imgs/$d->foto",['class'=>'img-responsive']);
                }
            ],
            'delegacion',
            //'delegacion0.nombre',
            
            [
                'attribute'=>'nombredelegacion',
                'value'=>'delegacion0.nombre'
            ],
            [
                'attribute'=>'nombrepoblacion',
                'value'=>'delegacion0.poblacion'
            ],
            

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view}{update}{delete}{delegacion}',
                'buttons'=>[
                    
                    'delegacion'=>function($url,$model){
                return Html::a("ver delegacion",["delegacion/view","id"=>$model->delegacion]);
                    }
                ],

            ],
            
        ],
    ]); ?>
</div>
